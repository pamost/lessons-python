# Символы строки
# Example 1
genome = 'ATGG'
for i in range(4):
    print(genome[i])

# Example 2
genome = 'ATGG'
for i in genome:
    print(i)

# Дана геномная последовательность. Вывести,
# сколько раз в ней встречается нуклеотид цитозин (символ С).
# Вариант1: Входные данные CACCTGGAC, выходные данные 4
# Вариант2: Взодные данные GATTACA, вызодные данные 1
# 1 вариант решения
g = input()
c = 'C'
j = 0
for i in g:
    if i == c:
        j += 1
print(j)

# 2 вариант решения
g = input()
print(g.count('C'))
