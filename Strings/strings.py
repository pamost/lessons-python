# strings
print("239" < "30" and 239 < 30)
print("239" < "30" and 239 > 30)
print("239" > "30" and 239 < 30)
print("239" > "30" and 239 > 30)

# Example 1
a = 'string1'
b = 'string2'

print(a, b)
print(a + b)
print(a + '\n' + b)

print("123" + "42")
