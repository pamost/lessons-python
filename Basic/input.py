# input
name = input('Введите своё имя: ')
print('Привет,', name)

# Example 1
x = int(input())
y = int(input())
print(x * 60 + y)

# Example 2
x = int(input())
print(x // 60)
print(x % 60)

# Example 3
x = int(input())
h = int(input())
m = int(input())

# Example 4
print(((x % 60 + m) // 60) + (x // 60) + h)
print((x % 60 + m) % 60)
