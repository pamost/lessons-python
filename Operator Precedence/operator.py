# Operator Precedence (Приоритет операторов)
# Сверху вниз
# **                          Exponentiation (rise to the power)
# ~ + -                       Complement, unary plus and minus
# * / % //                    Multiply, divide, modulo and floor division
# + -                         Addition and subtraction
# >> <<                       Right and left bitwise shift
# &                           Bitwise 'AND'
# ^ |                         Bitwise exclusive 'OR' and regular 'OR'
# <= < > >=                   Comparison operators
# <> == !=                    Equality operators
# = %= /= //= -= += *= **=    Assignment operators
# is is not                   Identity operators
# in not in                   Membership operators
# not or and                  Logical operators
