# # Example 1 - id
# x = [1, 2, 3]
# print(id(x))
# print(id([1, 2, 3]))
#
# # Example 2 - is
# z = [1, 2, 3]
# y = z
# print(y is z)
# print(y is [1, 2 ,3])
#
# # Example 3
# x = [1, 2, 3]
# y = x
# print(y is x)
# x.append(4)
# print(x)
# print(y)
# print(y is x)

# Example 4
x = [1, 2, 3]
y = x
y.append(4)

s = '123'
t = s
t += '4'

print(str(x) + " " + s)

print(type(x))
print(type(s))

print(type(type(x)))

