# Booleans
# Example 1
a = int(input())
print(a > 0)

# Example 2
a = int(input())
print(a >= 10) and (a < 100)
print(10 <= a < 100)

# Example 3
x1, x2, x3 = False, True, False
print(not x1)
print(x2 and x3)
print(not x1 or x2 and x3)
print(x1 or x2)

# Example 4
a, b = 1, 1
print((a and b) or (not a) and (not b))

# Example 5
x = 5
y = 10
print((y > (x * x)) or ((y >= (2 * x)) and (x < y)))

# Example 6
a = True
b = False
print(a and b or not a and not b)
