# try/except
# Example 1
try:
    n1 = 7
    n2 = 0
    print(n1/n2)
    print('Done calculation')
except ZeroDivisionError:
    print('An error occured due to zero division')

# Example 2
try:
    n1 = 7
    n2 = 0
    print(n1/n2)
    print('Done calculation')
except:
    print('An error occured due to zero division')

# Example 2 - finally
try:
    print('hello')
    print(1/0)
except ZeroDivisionError:
    print('Divided by zero')
finally:
    print('This code will run no matter what')

# Example 3 - raise
print(1)
raise ValueError
print(2)

# Example 4
try:
    print(1/0)
except ZeroDivisionError:
    raise ValueError

# Example 5
name = '123'
raise NameError('Invalid name')

# Example 6 - assert
print(1)
assert 1 + 2 == 3
print(2)
assert 1 + 1 == 3
print(3)
