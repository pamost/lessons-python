import requests

# Example 1
rq = requests.get('http://pavelmostovoy.com')
print(rq.text)

# Example 2
url = 'http://pavelmostovoy.com'
par = {'key1': 'value', 'key': 'value2'}
r = requests.get(url, params=par)
print(r.url)

# Example 3 - Используйте функцию get для получения файла (имеет смысл
# вызвать метод strip к передаваемому параметру, чтобы убрать пробельные символы по краям).
# После получения файла вы можете проверить результат, обратившись к полю text.
# Если результат работы скрипта не принимается, проверьте поле url на правильность.
# Для подсчёта количества строк разбейте текст с помощью метода splitlines.
# В поле ответа введите одно число или отправьте файл, содержащий одно число.
rt = requests.get('https://stepic.org/media/attachments/course67/3.6.2/555.txt')
s = rt.text.strip()
print(len(s.splitlines()))

# Example 4 - Имеется набор файлов, каждый из которых, кроме последнего, содержит имя следующего файла.
# Первое слово в тексте последнего файла: "We".
# Скачайте предложенный файл. В нём содержится ссылка на первый файл из этого набора.
# Все файлы располагаются в каталоге по адресу:
# https://stepic.org/media/attachments/course67/3.6.3/
# Загрузите содержимое ﻿последнего файла из набора, как ответ на это задание.
url = 'https://stepic.org/media/attachments/course67/3.6.3/'
link = requests.get(url + '699991.txt')
s = link.text.strip()
while s != 'We':
    r = requests.get(url + s)
    s = r.text.strip().splitlines()
    s = s[0]
    print(s)
    if s == 'We are the champions, my friends,':
        break
