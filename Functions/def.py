# Functions (Функции)
# Example 1


def my_func():
    print('Hello')
my_func()


# Example 2


def f(n):
    return n * 10 + 5

print(f(f(f(10))))


# Example 3


def my_range(start, stop, step=1):
    res = []
    if step > 0:
        x = start
        while x < stop:
            res += [x]
            x += step
    elif step < 0:
        x = start
        while x > stop:
            res += [x]
            x += step
    return res

print(my_range(2, 7))


# Example 4


def f(x):
    if x <= -2:
        x = 1 - (x + 2) ** 2
    elif -2 < x <= 2:
        x = -(x / 2)
    elif 2 < x:
        x = (x - 2) ** 2 + 1
    return x

print(f(float(input())))


# Example 5


def modify_list(l):
    res = []
    for i in l:
        if i % 2 == 0:
            s = i // 2
            res.append(s)
    l.clear()
    l += res

# Example 6
lst = [10, 5, 8, 3]
modify_list(lst)
print(lst)
