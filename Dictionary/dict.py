# Dictionary
# Example 1 - set
s = set()   # пустое множество

# Example 2
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
print(basket)
print('orange' in basket)
print('python' in basket)

# Example 3 - операциии с множествами
s = set()
s.add('element')        # добавить в множество
s.remove('element')     # удалить из множества, если элемент есть удаляем если нет то ошибка
s.discard('element')    # удалить из множества, если элемент есть удаляем если нет то ошибки не будет
s.clear()               # удалить всё в множестве
len(s)

# Example 4
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
for x in basket:
    print(x)

# Example 5 - dictionary
d = {'a': 239, 10: 240}
print(d['a'])
print(d[10])

# Example 6 - операции со словарями
# dict = { }
# key in dict
# key not in dict
# dict[key] = value
# dict[key]
# dict.get(key)
# del dict[key]

# Example 7 - перебор элементов словаря
d = {'C': 14, 'A': 12, 'T': 9, 'G': 18}
for key in d:
    print(key, end=' ')             # ключ
print()
for key in d.keys():
    print(key, end=' ')             # ключ
print()
for value in d.values():
    print(value, end=' ')           # значения
print()
for key, value in d.items():
    print(key, value, end=' ')      # ключ - значение

# Example 8
def update_dictionary(d, key, value):
    if key in d:
        d[key] += [value]
    elif 2*key in d:
        d[key*2] += [value]
    else:
        d[key*2] = [value]

d = {}
print(update_dictionary(d, 1, -1))  # None
print(d)                            # {2: [-1]}
update_dictionary(d, 2, -2)
print(d)                            # {2: [-1, -2]}
update_dictionary(d, 1, -3)
print(d)                            # {2: [-1, -2, -3]}

# Example 9
s = input().lower().split()
d = {}
m = set(s)
for i in m:
    print(i, s.count(i))

# Example 10
def f(x):
    return x + 1

d = {}
n = int(input())
for i in (range(n)):
    x = int(input())
    if x in d:
        print(d.get(x))
    else:
        d[x] = f(x)
        print(d.get(x))
