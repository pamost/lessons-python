# if
# Example 1
x = int(input())

if x % 2 == 0:
    print('Четное')
else:
    print('Нечетное')

# Example 2
a = int(input())
b = int(input())

if b != 0:
    print(a/b)
else:
    print('Делить на 0 нелзя!')
    b = int(input('Введите новое значение: '))
    if b == 0:
        print('Вы скорее всего не адекватны и Вам не дано решить этот пример, поэтому выходим из программы.')
    else:
        print(a / b)

# Example 3
# a - рекомендуется спать a часов
# b - спать не более b часов
# h - длительность сна в часах
a = int(input())
b = int(input())
h = int(input())

if a <= b:
    if a <= h <= b:
        print('Это нормально')
    elif h < a:
        print('Недосып')
    elif h > b:
        print('Пересып')
else:
    print('Не корректный ввод')

# Example 4
n = int(input('Введите год: '))

if 1900 <= n <= 3000:
    if n % 4 == 0 and n % 100 != 0 or n % 400 == 0:
        print('Високосный')
    else:
        print('Обычный')
else:
    print('Год должен быть в дипазаоне от 1900 до 3000')
