# Lists (Списки)

# Example 1
users = ['Ivan', 'Maria', 'Alex']
print(len(users))
for user in users:
    print('Hello,' + user + '!')

# Example 2 - сложение +
users = ['Ivan', 'Maria', 'Alex']
guests = ['Anna', 'Marina', 'Oleg']
print(users + guests)

# Example 3  - умножение *
print([0, 1] * 4)

# Example 4 - in
guests = ['Anna', 'Marina', 'Oleg']
print('Marina' in guests)
print('Pavel' in guests)

# Example 5 - not
nums = [1, 2, 3]
print(not 4 in nums)
print(4 not in nums)
print(not 3 in nums)
print(3 not in nums)

# Example 6 - method append
nums = [1, 2, 3]
nums.append(4)
print(nums)
users = ['Ivan', 'Maria', 'Alex']
users += ['Vika', 'Roza']
print(users)

# Example 7 - function len
nums = [1, 3, 5, 2, 4]
print(len(nums))

# Example 8 - method insert
words = ['Python', 'fun']
index = 1
words.insert(index, 'is')
print(words)

# Example 9 - method index
letters = ['p', 'q', 'r', 's', 'p', 'u']
print(letters.index('r'))   # 2
print(letters.index('p'))   # 0
print(letters.index('z'))   # ValueError: 'z' is not in list

# Example 10
letters = ['p', 'q', 'r', 's', 'p', 'u']
print(max(letters))         # возвращает элмент списка с наибольшим значением
print(min(letters))         # возвращает элмент списка с наименьшим значением
print(letters.count('p'))   # возвращает кол-во упоминаний эдемента в списке
print(letters.remove('s'))  # удаляет объект из списка
print(letters.reverse())    # располагает элменты в обратном порядке
print(letters)

# Example 11 - range
numbers = list(range(10))
print(numbers)      # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# Example 12 - range
numbers = list(range(3, 8))
print(numbers)
print(range(10) == range(0, 10))

# Example 13 - range
numbers = list(range(5, 30, 2))
print(numbers)      # [5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29]

# Example 14 - for
words = ['hello', 'world', 'spam', 'eggs']
for word in words:
    print(word + '!')

# Example 15 - for
for i in range(5):
    print('hello!')

# Example 16
num = [1, 1, 2, 3, 5, 8, 13]
print(num[num[4]])  # Result: 8 (1. num[4] = 5; 2. num[5] = 8)

# Example 17
for i in range(10):
    if not i%2 == 0:
        print(i + 1)

# Example 18 - change
users = ['Ivan', 'Maria', 'Alex']
users[0] = 'Pavel'
print(users)

# Example 19 +=
students = ['Ivan', 'Masha', 'Sasha']
students += ['Olga']
students += 'Olga'
print(students)

# Example 20 - del
users = ['Ivan', 'Maria', 'Alex']
del users[2]
print(users)

# Example 21 - sorted and sort
users = ['Ivan', 'Maria', 'Alex']
print(sorted(users))
users = ['Ivan', 'Maria', 'Alex']
print(users.sort())

# Example 22 - revers and reversed
nums = [3, 4, 5, 6]
print(list(reversed(nums)))
users = ['Ivan', 'Maria', 'Alex']
print(users.reverse())

# Example 23
a = [1, 2, 3]
b = a
print(b)

a[1] = 10
print(b)

b[0] = 20
print(a)

a = [5, 6]
print(b)

# Example 24 - generation lists
a = [0] * 7
print(a)    # [0, 0, 0, 0, 0, 0, 0]

a = [0 for i in range(7)]
print(a)    # [0, 0, 0, 0, 0, 0, 0]

a = [i * i for i in range(7)]
print(a)    # [0, 1, 4, 9, 16, 25, 36]

a = [int(i) for i in input().split()]
print(a)

# Напишите программу, на вход которой подается одна строка с целыми числами.
# Программа должна вывести сумму этих чисел.
nums = [int(i) for i in input().split()]
sum = 0
for num in nums:
    sum += num
print(sum)

# Напишите программу, на вход которой подаётся список чисел одной строкой.
# Программа должна для каждого элемента этого списка вывести сумму двух его соседей.
# Для элементов списка, являющихся крайними, одним из соседей считается элемент,
# находящий на противоположном конце этого списка. Например, если на вход подаётся
# список "1 3 5 6 10", то на выход ожидается список "13 6 9 15 7" (без кавычек).
# Если на вход пришло только одно число, надо вывести его же.
# Вывод должен содержать одну строку с числами нового списка, разделёнными пробелом.
nums = [int(i) for i in input().split()]

if len(nums) == 1:
    print(nums[0])
else:
    for i in range(0, len(nums) - 1):
        print(nums[i - 1] + nums[i + 1], end=' ')
    print(nums[0] + nums[-2])

# Напишите программу, которая принимает на вход список чисел в одной строке и
# выводит на экран в одну строку значения, которые повторяются в нём более одного раза.
# Для решения задачи может пригодиться метод sort списка.
# Выводимые числа не должны повторяться, порядок их вывода может быть произвольным.
# Sample Input 1: 4 8 0 3 4 2 0 3, Sample Output 1: 0 3 4
items = [int(i) for i in input().split()]
items.sort()
n = 1
for i in range(len(items) - 1):
    if items[i] == items[i + 1]:
        n += 1
    else:
        if n > 1:
            print(items[i], end=' ')
            n = 1
if n > 1:
    print(items[-1], end='')

# Example 25 - двумерные списки
a = [1, 2, 3],[4, 5, 6],[7, 8 , 9]
print(a[1][0])

# Example 26 - генерация двумерного списка
n = 3
a = [[0] * n for i in range(n)]
b = [[0 for j in range(n)] for k in range(n)]
print(a)
print(b)
